package edu.upenn.cis455.mapreduce.worker;

import static spark.Spark.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sleepycat.je.DatabaseException;

import edu.upenn.cis.stormlite.DistributedCluster;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis455.mapreduce.RunJobRoute;
import edu.upenn.cis455.mapreduce.storage.StorageFactory;
import edu.upenn.cis455.mapreduce.storage.StorageInterface;
import spark.Spark;

/**
 * Simple listener for worker creation 
 * 
 * @author zives
 *
 */
public class WorkerServer {
    static Logger log = LogManager.getLogger(WorkerServer.class);

    static DistributedCluster cluster = new DistributedCluster();

    static List<TopologyContext> contexts = new ArrayList<>();

    static List<String> topologies = new ArrayList<>();
    
    static String master;
    
    static StatusSender sender;
    
    static List<String> eos = new ArrayList<>();
    
    static StorageInterface storage;
    
    static int port;
    static String status = "IDLE";
    static String job = "";
    static int keysRead = 0;
    static int keysWritten = 0;
    static List<String> results = new ArrayList<String>();

	static String storageSt;


    public WorkerServer(int myPort) throws MalformedURLException {

        log.info("Creating server listener at socket " + myPort);

        port(myPort);
        final ObjectMapper om = new ObjectMapper();
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        post("/definejob", (req, res) -> {
            WorkerJob workerJob;
            try {
                workerJob = om.readValue(req.body(), WorkerJob.class);

                try {
                    log.info("Processing job definition request " + workerJob.getConfig().get("job") +
                            " on machine " + workerJob.getConfig().get("workerIndex"));
                    workerJob.getConfig().put("storage", storageSt);
                    job = workerJob.getConfig().get("job");
                    
                    contexts.add(cluster.submitTopology(workerJob.getConfig().get("job"), workerJob.getConfig(), 
                            workerJob.getTopology()));
                    
                    sender.setContext(contexts.get(contexts.size()-1));
                    // Add a new topology
                    synchronized (topologies) {
                        topologies.add(workerJob.getConfig().get("job"));
                    }
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return "Job launched";
            } catch (IOException e) {
                e.printStackTrace();

                // Internal server error
                res.status(500);
                return e.getMessage();
            } 

        });

        post("/runjob", new RunJobRoute(cluster));
        
        get("/shutdown", (req, res) -> {
        	System.out.println("shutdown received");
        	shutdown();
        	return "";
        });

        post("/pushdata/:stream", (req, res) -> {
            try {
                String stream = req.params(":stream");
                log.debug("Worker received: " + req.body());
                Tuple tuple = om.readValue(req.body(), Tuple.class);

                log.debug("Worker received: " + tuple + " for " + stream);

                // Find the destination stream and route to it
                StreamRouter router = cluster.getStreamRouter(stream);

                if (contexts.isEmpty())
                    log.error("No topology context -- were we initialized??");

                TopologyContext ourContext = contexts.get(contexts.size() - 1);

                
                // Instrumentation for tracking progress
                if (!tuple.isEndOfStream()) {
                	router.executeLocally(tuple, ourContext, tuple.getSourceExecutor());
                } else {
                	if (!eos.contains(tuple.getSourceExecutor())) {
                		router.executeEndOfStreamLocally(ourContext, tuple.getSourceExecutor());
                		eos.add(tuple.getSourceExecutor());
                	}
                }          
                // Please look at StreamRouter and its methods (execute, executeEndOfStream, executeLocally, executeEndOfStreamLocally)
                
                
                return "OK";
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

                res.status(500);
                return e.getMessage();
            }

        });

    }

    public static void createWorker(Map<String, String> config) {
        if (!config.containsKey("workerList"))
            throw new RuntimeException("Worker spout doesn't have list of worker IP addresses/ports");

        if (!config.containsKey("workerIndex"))
            throw new RuntimeException("Worker spout doesn't know its worker ID");
        else {
            String[] addresses = WorkerHelper.getWorkers(config);
            String myAddress = addresses[Integer.valueOf(config.get("workerIndex"))];

            log.debug("Initializing worker " + myAddress);

            URL url;
            try {
                url = new URL(myAddress);

                new WorkerServer(url.getPort());
                
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static void shutdown() {
        synchronized(topologies) {
            for (String topo: topologies)
                cluster.killTopology(topo);
        }
        sender.shutdown();
        cluster.shutdown();
        storage.shutdown();
        stop();
        System.exit(0);
    }
    
    public static StorageInterface getStorage() {
    	return storage;
    }
    
    public static void setStatus(String st) {
    	status = st;
    }
    
    public static void incKeysRead() {
    	keysRead++;
    }
    
    public static void incKeysWritten() {
    	keysWritten++;
    }
    
    public static void addResult(String r) {
    	results.add(r);
    }

    /**
     * Simple launch for worker server.  Note that you may want to change / replace
     * most of this.
     * 
     * @param args
     * @throws IOException 
     */
    public static void main(String args[]) throws IOException {
    	org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis455", Level.DEBUG);
    	
        if (args.length < 3) {
            System.out.println("Usage: WorkerServer [port number] [master host/IP]:[master port] [storage directory]");
            System.exit(1);
        }

        port = Integer.valueOf(args[0]);
        master = args[1];
        

        System.out.println("Worker node startup, on port " + port);
        System.out.println("Master at " + args[1]);

        WorkerServer worker = new WorkerServer(port);
        
        storageSt = args[2];
        
        if (!Files.exists(Paths.get("./"+storageSt))) {
            try {
                Files.createDirectory(Paths.get("./"+storageSt));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try {
        	storage = StorageFactory.getDatabaseInstance("./"+storageSt);
		} catch (DatabaseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        sender = new StatusSender();
        Thread s = new Thread(sender);
        s.start();

    }
}
