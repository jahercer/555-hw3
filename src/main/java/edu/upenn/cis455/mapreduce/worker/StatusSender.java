package edu.upenn.cis455.mapreduce.worker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

import edu.upenn.cis.stormlite.TopologyContext;

public class StatusSender implements Runnable {
	boolean shutdown =false;
	
	TopologyContext context;
	
	public StatusSender() {}
	
	@Override
	public void run() {
		while (!shutdown) {
			try {
				String urlSt = "http://" + WorkerServer.master + 
						"/workerstatus?port="+WorkerServer.port+
						"&status="+(context == null || context.getState().equals(TopologyContext.STATE.INIT) ? "idle" : 
							context.getState().equals(TopologyContext.STATE.MAP) ? "mapping" : 
								context.getState().equals(TopologyContext.STATE.REDUCE) ? "reducing" :
									"waiting")+
						"&job="+WorkerServer.job+
						"&keysRead="+(context != null ? 
								(context.getState().equals(TopologyContext.STATE.INIT) ? 
										 "0" : context.getMapOutputs()) : "0")+
						"&keysWritten="+(context != null ? context.getReduceOutputs() : "0")+
						"&results=[";
				int i = 0;
				if (context != null) {
					for (String s : context.getSendOutputs().keySet()) {
						urlSt += "("+s+","+context.getSendOutputs().get(s)+")";
						if (++i >= 100) {
							urlSt += "]";
							break;
						} else {
							urlSt += ",";
						}
					}
				}
				if (i < 100) urlSt += "]"; 
				
				URL url = new URL(urlSt.replaceAll("\\s+",""));
				System.out.println("URL: " +urlSt);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("GET");
				System.out.println(conn.getResponseCode());
				
				if(conn.getResponseCode() != 200) {
					BufferedReader reader = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
	
				    StringBuilder builder = new StringBuilder();
				    String line;
				    while ((line = reader.readLine()) != null) {
				      builder.append(line);
				    }
				    System.out.println(builder.toString());
				}
				
				conn.disconnect();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				Thread.currentThread().sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public void setContext(TopologyContext c) {
		context = c;
	}
	
	public void shutdown() {
		this.shutdown = true;
	}

}
