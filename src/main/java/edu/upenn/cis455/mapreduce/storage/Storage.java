package edu.upenn.cis455.mapreduce.storage;

import java.util.List;
import java.util.Map;

import com.sleepycat.je.Environment;

public class Storage implements StorageInterface {

	private Environment env;
	
	public Storage(Environment env) {
		this.env = env;
	}

	@Override
	public void addValue(String id, String key, String value) {
		ReduceValues values = StorageFactory.getReduceMap().get(id);
		
		if (values == null) {
			values = new ReduceValues();
			values.addValue(key, value);
		} else {
			values.addValue(key, value);
		}
		
		StorageFactory.getReduceMap().put(id, values);
		
		
	}

	@Override
	public Map<String, ReduceValues> getKeys() {
		return StorageFactory.getReduceMap();
	}

	@Override
	public void shutdown() {
		StorageFactory.getReduceMap().clear();
		StorageFactory.getClassCatalog().close();
		StorageFactory.getReduceDb().close();
	}

	@Override
	public void clear() {
		StorageFactory.getReduceMap().clear();	
	}
	
	
	
}
