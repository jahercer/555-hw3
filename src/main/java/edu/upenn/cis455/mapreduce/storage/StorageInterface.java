package edu.upenn.cis455.mapreduce.storage;

import java.util.Map;

public interface StorageInterface {

	/**
     * Add a new value to Db
     */
    public void addValue(String id, String key, String value);
    
    public Map<String, ReduceValues> getKeys();

	public void shutdown();
	
	public void clear();
	
}
