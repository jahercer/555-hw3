package edu.upenn.cis455.mapreduce.storage;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.collections.StoredEntrySet;
import com.sleepycat.collections.StoredSortedMap;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.UUID;

public class StorageFactory {
	
	private static Environment env;
	private static final String CLASS_CATALOG = "java_class_catalog";
	private static final String REDUCE_STORE = "reduce_store";
	
	private static StoredClassCatalog javaCatalog;
	
	private static Database reduceDb;
    
    private static StoredSortedMap<String, ReduceValues> reduceMap;
	
    public static StorageInterface getDatabaseInstance(String directory) throws DatabaseException, FileNotFoundException {
        // TODO: factory object, instantiate your storage server
    	
    	EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setTransactional(true);
        envConfig.setAllowCreate(true);

        env = new Environment(new File(directory), envConfig);
        
        //Create Class Catalog
        DatabaseConfig dbConfig = new DatabaseConfig();
        dbConfig.setTransactional(true);
        dbConfig.setAllowCreate(true);
        Database catalogDb = env.openDatabase(null, CLASS_CATALOG, dbConfig);
        javaCatalog = new StoredClassCatalog(catalogDb);
        
        //Initialize our DBs
        reduceDb = env.openDatabase(null, REDUCE_STORE, dbConfig);
        
        //Create bindings
        EntryBinding reduceKeyBinding = new SerialBinding(javaCatalog, String.class);
        EntryBinding reduceDataBinding = new SerialBinding(javaCatalog, ReduceValues.class);
        
        //Initialize Maps
        reduceMap = new StoredSortedMap(reduceDb, reduceKeyBinding, reduceDataBinding, true);
    	
        return new Storage(env);
    }

	public static final StoredClassCatalog getClassCatalog() {
		return javaCatalog;
	}

	public static Database getReduceDb() {
		return reduceDb;
	}

	public static final StoredSortedMap<String, ReduceValues> getReduceMap() {
		return reduceMap;
	}
	
	public final StoredEntrySet getReduceEntrySet() {
        return (StoredEntrySet) reduceMap.entrySet();
    }
	
}
