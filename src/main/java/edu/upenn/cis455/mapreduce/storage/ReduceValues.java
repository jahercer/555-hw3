package edu.upenn.cis455.mapreduce.storage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReduceValues implements Serializable {


	private static final long serialVersionUID = 1L;
	
	Map<String, List<String>> keys = new HashMap<String, List<String>>();
	
	public ReduceValues() {}
	
	public List<String> getValues(String key) {
		return keys.get(key);
	}
	
	public Map<String, List<String>> getWords() {
		return keys;
	}
	
	public void addValue(String key, String value) {
		if (keys.get(key) == null ) {
			keys.put(key, new ArrayList<String>());
		}
		keys.get(key).add(value);
	}
}
