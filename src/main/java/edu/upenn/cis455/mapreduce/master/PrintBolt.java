package edu.upenn.cis455.mapreduce.master;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.distributed.ConsensusTracker;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis455.mapreduce.storage.StorageInterface;
import edu.upenn.cis455.mapreduce.worker.WorkerServer;

/**
 * A trivial bolt that simply outputs its input stream to the
 * console
 * 
 * @author zives
 *
 */
public class PrintBolt implements IRichBolt {
	static Logger log = LogManager.getLogger(PrintBolt.class);
	
	Fields myFields = new Fields();

	ConsensusTracker votesForEos;
	
	TopologyContext context;
	
	FileWriter writer;
	
    /**
     * To make it easier to debug: we have a unique ID for each
     * instance of the PrintBolt, aka each "executor"
     */
    String executorId = UUID.randomUUID().toString();

	StorageInterface database;

	@Override
	public void cleanup() {
		// Do nothing

	}

	@Override
	public boolean execute(Tuple input) {
		context.setState(TopologyContext.STATE.DONE);
		if (!input.isEndOfStream()) {
			try {
				writer.append("("+input.getStringByField("key")+","+input.getStringByField("value")+")\n");
				writer.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("PRINT BOLT!! ("+input.getStringByField("key")+","+input.getStringByField("value")+")");
		} else if (votesForEos.voteForEos(input.getSourceExecutor())) {
			context.setState(TopologyContext.STATE.INIT);
			database.clear();
		}
		
		return true;
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		this.context = context;
		String output = stormConf.get("output");
		this.database = WorkerServer.getStorage();
		
		String path = ((String) stormConf.get("storage")).equals("") ? "." : "./"+(String) stormConf.get("storage") 
		+ (((String) stormConf.get("output")).equals("") ? "/" : "/"+(String) stormConf.get("output"));
	
		if (output != null && !Files.exists(Paths.get(path))) {
            try {
                Files.createDirectory(Paths.get(path));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
		if (!Files.exists(Paths.get(path+"/output.txt"))) {
			try {
                Files.createFile(Paths.get(path+"/output.txt"));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
		}
		
		try {
			writer = new FileWriter(path+"/output.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int numWorkers = Integer.valueOf(stormConf.get("numWorkers"));
        votesForEos = new ConsensusTracker(Integer.valueOf(stormConf.get("reduceExecutors"))*numWorkers);
        System.out.println("PRINT: " + (Integer.valueOf(stormConf.get("reduceExecutors"))*numWorkers));
	}

	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void setRouter(StreamRouter router) {
		// Do nothing
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(myFields);
	}

	@Override
	public Fields getSchema() {
		return myFields;
	}

}
