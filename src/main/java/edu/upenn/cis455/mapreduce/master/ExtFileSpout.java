package edu.upenn.cis455.mapreduce.master;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import edu.upenn.cis.stormlite.spout.FileSpout;

public class ExtFileSpout extends FileSpout {

	@Override
	public String getFilename(String path) throws FileNotFoundException {
		String locPath;
		if (!path.equals("")) {
			locPath = path;
		} else {
			locPath = ".";
		}
		
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(locPath))) {
	       for (Path entry: stream) {
	    	   System.out.println("Doc: "+entry.toString());
	    	   return entry.toString().substring(0, entry.toString().lastIndexOf('.'));		  
	       }
	   } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return path+"/input.in";
	}

}
