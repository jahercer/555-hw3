package edu.upenn.cis455.mapreduce.master;

import static spark.Spark.*;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.bolt.MapBolt;
import edu.upenn.cis.stormlite.bolt.ReduceBolt;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.spout.FileSpout;
import edu.upenn.cis.stormlite.tuple.Fields;

public class MasterServer {  
	static Logger log = LogManager.getLogger(MasterServer.class);
	
	private static final String WORD_SPOUT = "WORD_SPOUT";
    private static final String MAP_BOLT = "MAP_BOLT";
    private static final String REDUCE_BOLT = "REDUCE_BOLT";
    private static final String PRINT_BOLT = "PRINT_BOLT";
	
	static Map<String, String> workersInfo = new HashMap<String, String>();
	static Map<String, Date> workersDate = new HashMap<String, Date>();
	static Map<String, String> workersIndex = new HashMap<String, String>();
	
    public static void registerStatusPage() {
        get("/status", (request, response) -> {
            response.type("text/html");

            String res = "<html>\n"
            		+ "  <head>\n"
            		+ "    <title>Master</title>\n"
            		+ "  </head>\n"
            		+ "  <body>\n"
            		+ "		<header>"
            		+ "			<h1>Javier Hernandez Cerrillo - jahercer</h1>"
            		+ "		</header>"
            		+ "		<hr>";
            
            int i = 0;
            for (String s : workersInfo.keySet()) {
            	if (workersDate.get(s).getTime() + 30000 >= System.currentTimeMillis()) {
            		res += "<p>"+(i++)+": "+workersInfo.get(s)+"</p>";
            	}
            }
            
            res += "    <form method=\"POST\" action=\"/submitjob\">\n"
            	+ "      Job Name: <input type=\"text\" name=\"jobname\"/><br/>\n"
            	+ "      Class Name: <input type=\"text\" name=\"classname\"/><br/>\n"
            	+ "      Input Directory: <input type=\"text\" name=\"input\"/><br/>\n"
           		+ "      Output Directory: <input type=\"text\" name=\"output\"/><br/>\n"
           		+ "      Map Threads: <input type=\"text\" name=\"map\"/><br/>\n"
           		+ "      Reduce Threads: <input type=\"text\" name=\"reduce\"/><br/>\n"
           		+ "		 <input type=\"submit\" value=\"Submit job\"/>"
           		+ "      </form>\n"
          		+ "  </body>\n"
           		+ "</html>";
            
            return res;
        });

    }

    /**
     * The mainline for launching a MapReduce Master.  This should
     * handle at least the status and workerstatus routes, and optionally
     * initialize a worker as well.
     * 
     * @param args
     */
    public static void main(String[] args) {
    	org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis455", Level.DEBUG);
        if (args.length < 1) {
            System.out.println("Usage: MasterServer [port number]");
            System.exit(1);
        }

        int myPort = Integer.valueOf(args[0]);
        port(myPort);

        System.out.println("Master node startup, on port " + myPort);

        // TODO: you may want to adapt parts of edu.upenn.cis.stormlite.mapreduce.TestMapReduce here

        registerStatusPage();
        
        //workersInfo.put("0", "port=8001, status=IDLE, job=None, keysRead=0, keysWritten=0, results=[]");
        //workersInfo.put("1", "port=8002, status=IDLE, job=Foo, keysRead=2, keysWritten=2, results=[(a, 1),(b,1)]");
        
        // TODO: route handler for /workerstatus reports from the workers
        get("/workerstatus", (req, res) -> {
        	String info = "port="+req.queryParams("port")+
        			", status="+req.queryParams("status")+
        			", job="+req.queryParams("job")+
        			", keysRead="+req.queryParams("keysRead")+
        			", keysWritten="+req.queryParams("keysWritten")+
        			", results="+req.queryParams("results");
        	
        	System.out.println(req.ip()+":"+req.queryParams("port"));
        	workersInfo.put(req.ip()+":"+req.queryParams("port"), info);
        	workersDate.put(req.ip()+":"+req.queryParams("port"), new Date(System.currentTimeMillis()));
        	System.out.println(info);
        	return "Info added!";
        });
        
        post("/submitjob", (req, res) -> {
        	//Create topology
        	FileSpout spout = new ExtFileSpout();
 	        MapBolt bolt = new MapBolt();
 	        ReduceBolt bolt2 = new ReduceBolt();
 	        PrintBolt printer = new PrintBolt();

 	        TopologyBuilder builder = new TopologyBuilder();

 	        // Only one source ("spout") for the words
 	        builder.setSpout(WORD_SPOUT, spout, 1);
 	        
 	        // Parallel mappers, each of which gets specific words
 	        builder.setBolt(MAP_BOLT, bolt, Integer.valueOf(req.queryParams("map"))).fieldsGrouping(WORD_SPOUT, new Fields("value"));
 	        
 	        // Parallel reducers, each of which gets specific words
 	        builder.setBolt(REDUCE_BOLT, bolt2, Integer.valueOf(req.queryParams("reduce"))).fieldsGrouping(MAP_BOLT, new Fields("key"));

 	        // Only use the first printer bolt for reducing to a single point
 	        builder.setBolt(PRINT_BOLT, printer, 1).firstGrouping(REDUCE_BOLT);
 	        
 	        Topology topo = builder.createTopology();
 	        
 	        //Create config
 	        Config config = new Config();
 	        
 	        //Workers list
 	        String[] workers = workersInfo.keySet().toArray(new String[workersInfo.keySet().size()]);
 	        
 	        System.out.println(Arrays.toString(workers).replaceAll("\\s+",""));
 	        config.put("workerList", Arrays.toString(workers).replaceAll("\\s+",""));
 	        config.put("numWorkers", workers.length+"");
 	        
 	        //Job Name
 	        config.put("job", req.queryParams("jobname"));
 	        
 	        // Class with map function
 	        config.put("mapClass", req.queryParams("classname"));
 	        // Class with reduce function
 	        config.put("reduceClass", req.queryParams("classname"));
 	        
 	        // Numbers of executors (per node)
 	        config.put("spoutExecutors", "1");
 	        config.put("mapExecutors", req.queryParams("map"));
 	        config.put("reduceExecutors", req.queryParams("reduce"));
 	        
 	        config.put("input", req.queryParams("input"));
 	        config.put("output", req.queryParams("output"));
        	
 	        WorkerJob job = new WorkerJob(topo, config);
 	        
 	        System.out.println("INPUT: " + req.queryParams("input") + " || " + "OUTPUT: " + req.queryParams("output"));
 	        
			
	        ObjectMapper mapper = new ObjectMapper();
	        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
 	        
 	        try {
	 	        int i = 0;
	        	for (String s : workers) {
	        		config.put("workerIndex", (i++)+"");
	        		if (sendJob(s, "POST", config, "definejob", 
							mapper.writerWithDefaultPrettyPrinter().writeValueAsString(job)).getResponseCode() != 
							HttpURLConnection.HTTP_OK) {
						throw new RuntimeException("Job definition request failed");
					}
	        	}
	        	
	        	for (String s: workers) {
					if (sendJob(s, "POST", config, "runjob", "").getResponseCode() != 
							HttpURLConnection.HTTP_OK) {
						throw new RuntimeException("Job execution request failed");
					}
				}
 	        } catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		        System.exit(0);
			} 
        		
        	
        	return "";
        });
        
        get("/shutdown", (req, res) -> {
        	String[] workers = workersInfo.keySet().toArray(new String[workersInfo.keySet().size()]);
        	for (String s: workers) {
        		System.out.println("worker at: "+s);
        		URL url = new URL("http://" +s+ "/shutdown");
        		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        		conn.setDoOutput(true);
        		conn.setRequestMethod("GET");
        		try {
        		conn.getResponseCode();
        		} catch (ConnectException e){
        			System.out.println("Worker closed");
        		}
        		conn.disconnect();
			}
        	
        	stop();
        	//System.exit(0);
        	
        	return "";
        });
    }
    
    static HttpURLConnection sendJob(String dest, String reqType, Config config, String job, String parameters) throws IOException {
		URL url = new URL("http://" +dest+ "/" + job);

		log.info("Sending request to " + url.toString());
		
		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod(reqType);
		
		if (reqType.equals("POST")) {
			conn.setRequestProperty("Content-Type", "application/json");
			OutputStream os = conn.getOutputStream();
			byte[] toSend = parameters.getBytes();
			os.write(toSend);
			os.flush();
		}
		System.out.println(conn.getResponseCode());
		
		return conn;
    }
}

